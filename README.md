# pgcat

Um pacote Helm para o _database connection pooler_ [pgcat](https://github.com/postgresml/pgcat/).

- [Secrets](#secrets)
- [Parâmetros](#par%C3%A2metros)

## Secrets

É necessário escrever o arquivo de configuração `pgcat.toml` de acordo com as necessidades de cada cluster ou aplicação. Este chart espera que um Secret seja criado com o nome `pgcat-config`, esse nome pode ser alterado no `values.yaml`.

Segue um exemplo de configuração do pgcat. Atenção aos campos que começam com o prefixo "SUBSTITUIR_". Também é necessário ajustar, pelo menos, o campo `pool_size` de acordo com testes de carga (e, talvez, intuição). Outros campos estão documentados em <https://github.com/postgresml/pgcat/blob/main/CONFIG.md>.

```yaml
kubectl apply -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: pgcat-config
type: Opaque
stringData:
  pgcat.toml: |
    [general]
    host = "0.0.0.0"
    port = 6432
    enable_prometheus_exporter = true
    prometheus_exporter_port = 9930
    worker_threads = 5
    admin_username = "SUBSTITUIR_POR_UM_ADMIN_USERNAME"
    admin_password = "SUBSTITUIR_POR_UM_ADMIN_PASSWORD"

    [pools.ej]
    default_role = "primary"
    connect_timeout = 1000 # ms
    # If Query Parser is enabled, we'll attempt to parse
    # every incoming query to determine if it's a read or a write.
    # If it's a read query, we'll direct it to a replica. Otherwise, if it's a write,
    # we'll direct it to the primary.
    query_parser_enabled = false

    [pools.ej.users.0]
    username = "SUBSTITUIR_POR_USERNAME_DO_EJ"
    password = "SUBSTITUIR_POR_PASSWORD_DO_EJ"
    # Maximum number of server connections that can be established for this user.
    # The maximum number of connection from a single Pgcat process to any database in the cluster is the sum of pool_size across all users.
    pool_size = 200

    [pools.ej.shards.0]
    servers = [
        [ "SUBSTITUIR_POR_HOST_DO_EJ", SUBSTITUIR_POR_PORT_DO_EJ, "primary" ]
    ]
    database = "SUBSTITUIR_POR_DB_DO_EJ"

    [pools.rasa]
    default_role = "primary"
    connect_timeout = 1000 # ms
    query_parser_enabled = false

    [pools.rasa.users.0]
    username = "SUBSTITUIR_POR_USERNAME_DO_RASA"
    password = "SUBSTITUIR_POR_PASSWORD_DO_RASA"
    pool_size = 200

    [pools.rasa.shards.0]
    servers = [
        [ "SUBSTITUIR_POR_HOST_DO_RASA", SUBSTITUIR_POR_PORT_DO_RASA, "primary" ]
    ]
    database = "SUBSTITUIR_POR_DB_DO_RASA"
EOF
```

## Parâmetros

A seguinte tabela lista os parâmetros configuráveis do chart e seus valores padrão.

| Parâmetro                | Descrição             | Padrão        |
| ------------------------ | ----------------------- | -------------- |
| `app.name` |  | `"pgcat"` |
| `app.kind` | Recurso utilizado para fazer o deploy do `pgcat`. Recomendados: DaemonSet ou Deployment. | `"Deployment"` |
| `app.replicas` | Quantidade de réplicas em caso de `app.kind` ser Deployment. | `1` |
| `app.resources.limits.memory` |  | `"1Gi"` |
| `app.resources.limits.cpu` |  | `"2000m"` |
| `app.resources.requests.memory` |  | `"1Gi"` |
| `app.resources.requests.cpu` |  | `"1000m"` |
| `app.image` | Imagem docker do `pgcat`. A tag da imagem corresponde ao hash do commit da release. Consultar releases em <https://github.com/postgresml/pgcat/releases>. | `"ghcr.io/postgresml/pgcat:1f2c6507f7fb5461df1a599c0b380aa114597bb5"` |
| `app.config.existingSecret.name` | Nome do Secret que contém as configurações do `pgcat`. | `"pgcat-config"` |
